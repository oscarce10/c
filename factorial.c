#include <stdio.h>
#include <cs50.h>
#include <stdlib.h>

long long int fact(int n);

int main(int argc, char *argv[]){

    if (argc == 1)    {
        int n = get_int("Ingrese numero: ");
        printf("El factorial de %i es: %lli\n", n, fact(n));
    }

    else
        // string to long(string, endpointer, base)
        printf("El factorial de %s es: %lli\n", argv[1], fact(strtol(argv[1], NULL, 10)));
}

long long int fact(int n)
{
    return (n == 1 || n == 0) ? 1 : n * fact(n - 1);
}