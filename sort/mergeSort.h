
void mergeSort(int low, int high, int *v, int *aux);
void merge(int low, int middle, int high, int *v, int *aux);

void mergeSort(int low, int high, int *v, int *aux)
{
    if (low < high)
    {
        int middle = low + (high - low) / 2;
        mergeSort(low, middle, v, aux);
        mergeSort(middle + 1, high, v, aux);
        merge(low, middle, high, v, aux);
    }
}

void merge(int low, int middle, int high, int *v, int *aux)
{
    for (int i = 0; i <= high; i++)
    {
        aux[i] = v[i];
    }

    int i, j, k;
    i = k = low, j = middle + 1;

    while (i <= middle && j <= high){
        if (aux[i] <= aux[j])
        {
            v[k] = aux[i];
            i++;
        }
        else
        {
            v[k] = aux[j];
            j++;
        }
        k++;
    }

    while (i <= middle)
    {
        v[k] = aux[i];
        k++;
        i++;
    }

}