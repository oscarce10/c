#include <stdio.h>
#include <stdlib.h>
#include "mergeSort.h"
#define MAX 10

int main()
{
    int v[MAX] = {3, 6, 8, 0, 1, 2, 7, 5, 9, 4};
    int *aux = malloc(MAX * sizeof(int));
    printf("Before sorting\n");
        for(int i = 0; i<MAX; i++){
                printf(((i+1 != MAX)?"%i - ":"%i\n"), v[i]);
        }
    mergeSort(0, MAX - 1, v, aux);
    printf("After sorting\n");
        for(int i = 0; i<MAX; i++){
                printf(((i+1 != MAX)?"%i - ":"%i\n"), v[i]);
        }
    free(aux);
    return 0;
}