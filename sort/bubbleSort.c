/* Move higher valued elements generally towards the right and lower value 
 * elements generally towards left. */

#include <stdio.h>
#define MAX 10

int main(){	 	 
	int v[MAX] = {7, 5, 9, 8, 1, 3, 2, 4, 6, 10};
	int vMax = MAX;

        printf("Before sorting\n");
        for(int i = 0; i<MAX; i++){
                printf(((i+1 != MAX)?"%i - ":"%i\n"), v[i]);
        }

	int swapCount = 1;
	while(swapCount != 0){
		swapCount = 0;
		for(int i = 0; i<vMax; i++){
			for(int j = i; j<vMax; j++){
				if(v[i] < v[j]){
					int aux = v[i];
					v[i] = v[j];
					v[j] = aux;
					swapCount++;
				}
			}
		}
		vMax--;
	}

	printf("After sorting\n");
        for(int i = 0; i<MAX; i++){
                printf(((i+1 != MAX)?"%i - ":"%i\n"), v[i]);
        }

}
