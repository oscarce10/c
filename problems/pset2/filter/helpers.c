#include <stdio.h>
#include "helpers.h"

// Convert image to grayscale
void grayscale(int height, int width, RGBTRIPLE image[height][width])
{
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            int prom = (image[i][j].rgbtBlue + image[i][j].rgbtGreen + image[i][j].rgbtRed) / 3;
            image[i][j].rgbtGreen = image[i][j].rgbtRed = image[i][j].rgbtBlue = prom;
        }
    }
    return;
}

// Convert image to sepia
void sepia(int height, int width, RGBTRIPLE image[height][width])
{
    int c = 0;
    FILE *pix = fopen("pixels.txt", "a");
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){

            int red = image[i][j].rgbtRed, green = image[i][j].rgbtGreen, blue = image[i][j].rgbtBlue;
            int sepiaRed, sepiaGreen, sepiaBlue;
            sepiaRed = (.393 * red + .769 * green + .189 * blue);
            sepiaGreen = (.349 * red + .686 * green + .168 * blue);
            sepiaBlue = (.272 * red + .534 * green + .131 * blue);

            if(sepiaRed > 255){
                sepiaRed = 255;
            }
            if(sepiaGreen > 255){
                sepiaGreen = 255;
            }

            if(sepiaBlue > 255){
                sepiaBlue = 255;
            }


            image[i][j].rgbtGreen = sepiaGreen;
            image[i][j].rgbtRed = sepiaRed;
            image[i][j].rgbtBlue = sepiaBlue;
        }
    }
    printf("Width: %i\nHeight: %i\n", width, height);
    return;
}

// Reflect image horizontally
void reflect(int height, int width, RGBTRIPLE image[height][width])
{
    printf("Width: %i\nHeigth: %i\n", width, height);
    for(int i = 0; i < height; i++){
        for(int j = 0, k = width - 1; j <= k; j++, k--){
            //printf("i = %i\nj = %i\nk = %i\n", i, j, k);
            RGBTRIPLE aux = image[i][k];
            image[i][k] = image[i][j];
            image[i][j] = aux;
        }
    }
    return;
}

// Blur image
void blur(int height, int width, RGBTRIPLE image[height][width])
{
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            int sumRed = 0, sumGreen = 0, sumBlue = 0, c = 0;
            if(i - 1 >= 0){
                if(j - 1 >= 0){
                    sumBlue  +=  image[i - 1][j - 1].rgbtBlue;
                    sumGreen +=  image[i - 1][j - 1].rgbtGreen;
                    sumRed   +=  image[i - 1][j - 1].rgbtRed;
                    c++;
                }

                sumBlue  +=  image[i - 1][j].rgbtBlue;
                sumGreen +=  image[i - 1][j].rgbtGreen;
                sumRed   += image[i - 1][j].rgbtRed;
                c++;

                if(j + 1 < width){
                    sumBlue  +=  image[i - 1][j + 1].rgbtBlue;
                    sumGreen +=  image[i - 1][j + 1].rgbtGreen;
                    sumRed   += image[i - 1][j + 1].rgbtRed;
                    c++;
                }
            }

            if(j - 1 >= 0){
                sumBlue  +=  image[i][j - 1].rgbtBlue;
                sumGreen +=  image[i][j - 1].rgbtGreen;
                sumRed   += image[i][j - 1].rgbtRed;
                c++;
            }

            sumBlue  +=  image[i][j].rgbtBlue;
            sumGreen +=  image[i][j].rgbtGreen;
            sumRed   += image[i][j].rgbtRed;
            c++;

            if(j + 1 < width){
                sumBlue  +=  image[i][j + 1].rgbtBlue;
                sumGreen += image[i][j + 1].rgbtGreen;
                sumRed   += image[i][j + 1].rgbtRed;
                c++;
            }

            if(i + 1 < height){
                if(j - 1 >= 0){
                    sumBlue  +=  image[i + 1][j - 1].rgbtBlue;
                    sumGreen +=  image[i + 1][j - 1].rgbtGreen;
                    sumRed   += image[i + 1][j - 1].rgbtRed;
                    c++;
                }

                sumBlue  +=  image[i + 1][j].rgbtBlue;
                sumGreen +=  image[i + 1][j].rgbtGreen;
                sumRed   += image[i + 1][j].rgbtRed;
                c++;

                if(j + 1 < width){
                    sumBlue  +=  image[i + 1][j + 1].rgbtBlue;
                    sumGreen +=  image[i + 1][j + 1].rgbtGreen;
                    sumRed   += image[i + 1][j + 1].rgbtRed;
                    c++;
                }
            }
            image[i][j].rgbtRed = sumRed / c;
            image[i][j].rgbtGreen = sumGreen / c;
            image[i][j].rgbtBlue = sumBlue / c;
        }
    }
    return;
}
