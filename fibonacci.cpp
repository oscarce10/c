//Fibonacci Series using Recursion
#include <iostream>
using namespace std;

int fib(int n)
{
    if (n == 0)
        return 0;
    else if (n == 1 || n == 2)
        return 1;
    else
        return fib(n - 1) + fib(n - 2);
}

int main ()
{
    int n = -1;
    cout<<"Ingrese numero: ";
    cin>>n;
    int *v = (int *)malloc(sizeof(int) * n);
    for(int i = 0; i < n; i++){
        v[i] = fib(i);
    }
    for(int i = 0; i < n; i++){
        cout<<v[i]<<" ";
    }
    cout<<endl;
    return 0;

}