
/* Find the smallest unsorted element and add it to the end of the sorted list */

/* Best case = Worst case: n**2 */

#include <stdio.h>

#define MAX 10



int main(){

	int v[MAX] = {7, 5, 9, 8, 1, 3, 2, 4, 6, 10};

	printf("Before sorting\n");
	for(int i = 0; i<MAX; i++){
		printf(((i+1 != MAX)?"%i - ":"%i\n"), v[i]);
	}

	printf("After sorting\n");
	for(int i = 0; i<MAX; i++){
		for(int j=i; j<MAX; j++){
			if(v[j]<v[i]){
				int aux = v[i];
				v[i] = v[j];
				v[j] = aux;
			}
		}
	}

	for(int i = 0; i<MAX; i++){
                printf(((i+1 != MAX)?"%i - ":"%i\n"), v[i]);
        }
}

