#include <stdio.h>
#include <string.h>
void swap(int a, int b);

int main(){
    int x = 1;
    int y = 5;
    printf("x: %p\n", x);
    printf("y: %p\n", y);
    swap(x, y);
    printf("x: %i\n", x);
    printf("y: %i\n", y);
}

void swap(int a, int b){
    int tmp = a;
    a = b;
    b = tmp;
}