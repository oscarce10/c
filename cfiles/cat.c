/* Simulation of cat command used in Linux terminal */
#include <stdio.h>
#include <stdlib.h>

int main(){
    FILE* ptr = fopen("../argv1.c", "r");
    if(ptr){
    char ch;
    while((ch = fgetc(ptr)) != EOF)
        printf("%c", ch);

    fclose(ptr);
    }

    else{
        printf("Archivo no localizado\n");
        return 1;
    }
    return 0;
}/* this is a line added with fwrite */
