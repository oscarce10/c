#include <stdion.h>
#include <cs50.h>

/* File pointers */

/* ---------------------------------------------------------------------- */
/* fopen() */
    // Opens a fiile and returns pointer to it.
    // Always check the return value to make sure you don't get back NULL.

// FILE* ptr = fopen(<filename>, <operation: r - w - a>);

// FILE* ptr = fopen("ptr1", "w");

/* ----------------------------------------------------------------------- */

/* fclose() */
    // Closes the file pointed to by given file pointer

// fclose(<file pointer>);

// fclose(ptr1);

/* ----------------------------------------------------------------------- */

/* fgetc() */
    // Reads and returns the next character from the file pointed to.
    // Note: The operation of the file pointer passed in as a parameter
    //  must be "r" for read or you will suffer an error.

// 1. fopen("ptr1", "r");
// 2. char ch = fgetc(<file pointer>);

// char ch = fgetc(ptr1);

/* Example of cat function of Linux
* The ability to get single characters from files, if wrapped in a loop
* means we could read all the characters from a file and print them
* to the screen, one-by-one */
// cat <file name>

// char ch;
// while ((ch = fget(ptr)) != EOF)
    // printf("%c", ch);

// SEE cat.c

/* ----------------------------------------------------------------------- */

/* fputc() */
    // Writes or appends the specified character to the pointed-file
    /* Note: The operation of the file pointer passed in as a parameter
     * must be "w" for write or "a" for append, or you will suffer an error */

// 1. fopen(ptr, <"w" "a">);
// 2. fputc(<character>, <file pointer>);

// fputc('!', ptr);

// SEE cp.c

/* ----------------------------------------------------------------------- */

/* fread() */

    /* Reads <qty> units of size <size> from the file pointed to
     * and stores them in memory in a buffer (usually an array)
     * pointed to by <buffer> */

    /* Note: The operation of the file pointer passed in as a
     * parameter must be "r" for read, or you will suffer an error */

// 1. fopen(ptr, "r");
// 2. fread(<buffer>, <size>, <qty>, <file pointer>);
        /* buffer: a pointer to the location where we're going
           to store information; array, file, variable, etc.
           size: How large each unit of information will be.
           qty: How many units of information we want to acquire
           file pointer: file we want to get them */

// SEE fread.c

/* ----------------------------------------------------------------------- */

/* fwrite() */

    /* Writes <qty> units of size <size> to the file pointed to by
     * reading them from a buffer (usually an array) pointed to by
     * <buffer> */

     /* Note: The operation of the file pointer passed in as a
      * parameter must be "w" for write or "a" for append */

// 1. fopen(ptr, <"w"> <"a">);
// 2. fwrite(<file pointer>, <size>, <qty>, <buffer>);