#include <stdio.h>
#include <stdlib.h>
#include <cs50.h>
#include <string.h>

int main(){
    char *ptr = "/* this is a line added with fwrite */\n";
    FILE *buffer = fopen("cat.c", "a");
    fwrite(ptr, sizeof(char), strlen(ptr), buffer);
    fclose(buffer);
}