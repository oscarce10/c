/* Simulation of cp command used in Linux terminal */

/* Now we can read characters from files and write charactes to them
 * Here we are going to copy one file to another */

#include <stdio.h>
#include <stdlib.h>

int main(){
    FILE* ptr = fopen("../argv0.c", "r");
    if(ptr){
        FILE *ptr2 = fopen("argv0copy.c", "a");
        if(ptr2){
        char ch;
        while((ch = fgetc(ptr)) != EOF)
            fputc(ch, ptr2);

        fclose(ptr);
        fclose(ptr2);
        printf("Archivo copiado satisfactoriamente\n");
        }
        else{
            printf("Error al crear archivo copia\n");
            return 1;
        }
    }
    else{
        printf("Archivo no localizado\n");
        return 1;
    }
    return 0;
}