#include <stdio.h>
#include <stdlib.h>
#include <cs50.h>

int main(){
    char *buffer = malloc(sizeof(char) * 250);
    FILE *ptr = fopen("cat.c", "r");
    fread(buffer, sizeof(char), 250, ptr);
    printf("%s\n", buffer);


    //fread() as fgetsc()
    puts("fread() as fgetsc()");
    char c;
    fread(&c, sizeof(char), 1, ptr);
    printf("Char: %c\n", c);
    printf("%ld\n", ftell(ptr));
    fclose(ptr);
}