// Run program with valgrind ./valgrind OR help50 valgrind ./valgrind


#include <stdio.h>
#include <stdlib.h>

void f();

int main(){
    f();
}

void f(){
    int *x = malloc(10 * sizeof(int));
    // This return a chunk of memory within an array.
    x[0] = 45;
    x[10] = 2;
    printf("Numbers:\n[0]:%i\n[10]:%i\n", x[0], x[10]);
}