#include <stdio.h>

int main(){
    int k;
    k = 5;
    int *pk;
    pk = &k;
    printf("Address of k: &k = %p\n", &k);
    printf("Value of k: k = %i\n", k); // k is not a pointer
    printf("Address of pk: &pk = %p\n", &pk);
    printf("Value of pk: *pk = %i\n", *pk); // pk is a pointer
    *pk = 100;
    printf("Value of k after changed *pk = %i\n", k);

    // Change address which pk points to
    printf("Value of pk before changing address: *pk = %i\n", *pk);
    int m;
    m = 366;
    pk = &m;
    printf("Value of pk after changing address: *pk = %i\n", *pk);
}
