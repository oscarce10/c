#include <iostream>

#define A 54059 /* a prime */
#define B 76963 /* another prime */
#define C 86969 /* yet another prime */
#define FIRSTH 37 /* also prime */

using namespace std;

unsigned int hash_str(const char* s)
{
   unsigned int h = FIRSTH;
   while (*s) {
     h = (h * A) ^ (s[0] * B);
     s++;
   }
   return h % A; // or return h % C;
}

int main(){
   char *word = (char *)malloc(sizeof(char) * 50);
   cout<<"Inserte palabra: ";
   cin>>word;
   cout<<"El hash de "<<word<<" es: "<<hash_str(word)<<endl;
}