
using namespace std;

class ListaSim{
    int num;
    ListaSim *sig;

    public:
    void crearCabeza (ListaSim *&, int);
    void agregarNodo (ListaSim *&, int);
    void agregarLista (ListaSim *&, int);
    void mostrarLista (ListaSim *);
    void borrarNodo (ListaSim *&);
    void borrarLista (ListaSim *&);
    bool listaVacia (ListaSim *&);
}ListaSim;

void ListaSim::crearCabeza(ListaSim *&cabeza, int num){
    cabeza = new ListaSim;
    cabeza->num = num;
    cabeza -> sig = NULL;
}

void ListaSim::agregarNodo(ListaSim *&cabeza, int num){


    if(num < cabeza->num){
        ListaSim *aux = new ListaSim;
        aux->num = num;
        aux->sig = cabeza;
        cabeza = aux;
    }
    else{
        ListaSim *aux = cabeza;

        while (aux->sig){
            if(aux->sig->num <= num){
                if(aux->sig->num == num){
                    cout<<"Dato repetido, no se incluye en la lista"<<endl;
                    return;
                }else
                    aux = aux->sig;
            }else
                break;
        }

        if(aux->sig){
            ListaSim *aux2 = new ListaSim;
            aux2->num = num;
            aux2->sig = aux->sig;
            aux->sig = aux2;
        }else{
            aux->sig = new ListaSim;
            aux = aux->sig;
            aux->num = num;
            aux->sig = NULL;
        }
    }
}

void ListaSim::agregarLista(ListaSim *&cabeza, int num){
        (!cabeza)?crearCabeza(cabeza, num):agregarNodo(cabeza, num);
}

void ListaSim::mostrarLista(ListaSim *cabeza){
    if(!listaVacia(cabeza))
        return;
    else{
        cout<<"\n---------------------------------------\n";
        cout<<"\nDatos ingresados:\n";
        while(cabeza){
            cout<<cabeza->num<<" ";
            cabeza = cabeza->sig;
        }
        cout<<"\n---------------------------------------\n";
    }

}

void ListaSim::borrarNodo(ListaSim *&cabeza){
    if(!listaVacia(cabeza))
        return;
    cout<<"\n---------------------------------------\n";
    int del;
    cout<<"Inserte dato a eliminar: ";
    cin>>del;
    ListaSim *aux = cabeza;
    while(aux && aux->num != del){
        aux = aux->sig;
    }
    if(!aux){
        cout<<"Nodo no encontrado."<<endl;
        borrarNodo(cabeza);
    }else if (aux == cabeza){
        int d = aux->num;
        cabeza = cabeza->sig;
        delete aux;
        cout<<"Nodo de dato "<<d<<" eliminado, cabeza trasladada."<<endl;
    }else {
        ListaSim *aux2 = cabeza;
        while(aux2->sig != aux){
            aux2 = aux2->sig;
        }
        aux2->sig = aux->sig;
        int d = aux->num;
        delete aux;
        cout<<"Nodo de dato "<<d<<" eliminado."<<endl;
    }
    cout<<"\n---------------------------------------\n";
}

void ListaSim::borrarLista (ListaSim *&cabeza){
    if(!listaVacia(cabeza))
        return;
    // Eliminar nodo por nodo de forma recursiva
    if(cabeza)
        borrarLista(cabeza->sig);

    delete cabeza;
    cabeza = nullptr;
}

bool ListaSim::listaVacia(ListaSim *&cabeza){
    if(!cabeza){
        system("clear");
        cout<<"Lista vacia"<<endl;
        return false;
    }
    else
        return true;
}