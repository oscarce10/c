// Compile by using g++ -g  linkedList.cpp  -lcrypt -lcs50 -lm -o linkedList

#include <iostream>
#include "linkedList.h"


int main(){
    class ListaSim *cabeza = NULL, ob;
    while(true){
        char opc[1];
        cout<<"Menu:"<<endl;
        cout<<"1. Agregar un elemento a la lista"<<endl;
        cout<<"2. Agregar varios elementos a la lista"<<endl;
        cout<<"3. Ver lista"<<endl;
        cout<<"4. Eliminar elemento de la lista"<<endl;
        cout<<"5. Eliminar toda la lista"<<endl;
        cout<<"6. Salir"<<endl;
        cout<<"Seleccione opcion: ";
        cin>>opc;
        int opcion = atoi(opc);

        switch(opcion){
            case 1:{
                int dato = 0;
                system ("clear");
                cout<<"Inserte dato: ";
                cin>>dato;
                while(dato < 1){
                    cout<<"ingrese solo datos positivos"<<endl;
                    cout<<"Inserte dato: ";
                    cin>>dato;
                }
                ob.agregarLista(cabeza, dato);

            }break;

            case 2:{
                int datos;
                cout<<"Ingrese cantidad de datos a registrar en la cola: ";
                cin>>datos;
                int dato = 0;
                for(int i = 0; i < datos; i++){
                    cout<<"Inserte dato: ";
                    cin>>dato;
                    while(dato < 1){
                        cout<<"ingrese solo datos positivos"<<endl;
                        cout<<"Inserte dato: ";
                        cin>>dato;
                    }
                ob.agregarLista(cabeza, dato);
                }
            }break;

            case 3:
                system ("clear");
                ob.mostrarLista(cabeza);
                break;

            case 4:
                ob.borrarNodo(cabeza);
                break;

            case 5:
                ob.borrarLista(cabeza);
                break;

            case 6:
                system("clear");
                return 0;

            default:
                cout<<"Ingrese un valor valido"<<endl;
                break;
        }
    }
    return 0;
}

