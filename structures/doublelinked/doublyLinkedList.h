using namespace std;
class ListaD{
    int num;
    ListaD *sig, *ant;

    public:
    void crearCabeza (ListaD *&, int);
    void agregarNodo (ListaD *&, int);
    void crearLista (ListaD *&);
    void mostrarLista (ListaD *);
    void borrarNodo (ListaD *&);
}listaD;

void ListaD::crearCabeza(ListaD *&cabeza, int num){
    cabeza = new ListaD;
    cabeza->num = num;
    cabeza->sig = cabeza->ant = NULL;
}

void ListaD::agregarNodo(ListaD *&cabeza, int num){
    ListaD *aux = cabeza;

    while(aux->sig){
        aux = aux->sig;
    }

    aux->sig = new ListaD;
    aux->sig->ant = aux;
    aux = aux->sig;
    aux->num = num;
    aux->sig = NULL;
}

void ListaD::crearLista(ListaD *&cabeza){
    int num = 0;
    while(true){
        cout<<"Inserte numero: ";
        cin>>num;

        if(num < 1)
            break;

        (!cabeza)?crearCabeza(cabeza, num):agregarNodo(cabeza, num);
    }
}

void ListaD::mostrarLista(ListaD *cabeza){
    cout<<"\nDatos ingresados en orden:\n";
    ListaD *aux = cabeza;
    while(aux->sig){
        cout<<aux->num<<endl;
        aux = aux->sig;
    }
    cout<<"\nDatos ingresados reves:\n";
    while(aux){
        cout<<aux->num<<endl;
        aux = aux->ant;
    }
}

void ListaD::borrarNodo(ListaD *&cabeza){
    int del;
    cout<<"Inserte dato a eliminar: ";
    cin>>del;
    ListaD *aux = cabeza;
    while(aux && aux->num != del){
        aux = aux->sig;
    }
    if(!aux){
        cout<<"Nodo no encontrado."<<endl;
        borrarNodo(cabeza);
    }else if (aux == cabeza){
        int d = aux->num;
        cabeza = cabeza->sig;
        delete aux;
        cabeza->ant = NULL;
        cout<<"Nodo de dato "<<d<<" eliminado, cabeza trasladada a dato "<<cabeza->num<<endl;
    }else if(!aux->sig){
        int d = aux->num;
        aux->ant->sig = aux->sig;
        delete aux;
        cout<<"Nodo de dato "<<d<<" eliminado."<<endl;
    }else{
        int d = aux->num;
        aux->ant->sig = aux->sig;
        aux->sig->ant = aux->ant;
        delete aux;
        cout<<"Nodo de dato "<<d<<" eliminado."<<endl;
    }
}