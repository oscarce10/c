using namespace std;

class Cola{
    private:
        int dato;
        Cola *sig;

    public:
        void iniciarCola(Cola *&);
        void agregarElemento(Cola *&, int);
        void atenderElemento(Cola *&);
        bool colaVacia(Cola *);
        void mostrarCola(Cola *);
}Cola;

void Cola::iniciarCola(Cola *&cabeza){
    cabeza = new Cola;
    cabeza->dato  = NULL;
    cabeza->sig = NULL;
}

void Cola::agregarElemento(Cola *&cabeza, int dato){
    Cola *aux = cabeza;

    while(aux->sig)
        aux = aux->sig;

    aux->sig = new Cola;
    aux = aux->sig;
    aux->dato = dato;
    aux->sig = NULL;
    cout<<"Dato "<<dato<<" insertado a la cola"<<endl;
}

bool Cola::colaVacia(Cola *cabeza){
    return !(cabeza->sig) ? true : false;
}

void Cola::atenderElemento(Cola *&cabeza){
    if(!cabeza->sig){
        cout<<"Cola vacia\n";
    }else{
        Cola *aux = cabeza->sig;
        cabeza->sig = aux->sig;
        cout<<"Cola atendida\n";
        delete aux;
    }

}

void Cola::mostrarCola(Cola *cabeza){
    if(!cabeza->sig)
        cout<<"Cola vacia"<<endl;

    else{
        cout<<"Se muestra la cola\n";
        Cola *aux = cabeza->sig;
        while(aux){
            cout<<aux->dato<<" ";
            aux = aux->sig;
        }
        cout<<endl;
    }
}