/* The order is First In First Out (FIFO). -- Cola -- */
/* La cabeza no se tiene en cuenta dentro de los datos de la cola */

// Compile by using g++ -g  queue.cpp  -lcrypt -lcs50 -lm -o queue


#include <iostream>
#include <stdlib.h>
#include "queue.h"

int main(){
    class Cola *cabeza = NULL, ob;
    ob.iniciarCola(cabeza);
    while(true){
        char opc[1];
        cout<<"Menu:"<<endl;
        cout<<"1. Agregar un dato a la cola"<<endl;
        cout<<"2. Agregar varios datos a la cola"<<endl;
        cout<<"3. Ver cola"<<endl;
        cout<<"4. Atender cola"<<endl;
        cout<<"5. Salir"<<endl;
        cout<<"Seleccione opcion: ";
        cin>>opc;
        int opcion = atoi(opc);

        switch(opcion){
            case 1:{
                int dato = 0;
                system ("clear");
                cout<<"Inserte dato: ";
                cin>>dato;
                while(dato < 1){
                    cout<<"ingrese solo datos positivos"<<endl;
                    cout<<"Inserte dato: ";
                    cin>>dato;
                }
                ob.agregarElemento(cabeza, dato);

            }break;

            case 2:{
                int datos;
                cout<<"Ingrese cantidad de datos a registrar en la cola: ";
                cin>>datos;
                int dato = 0;
                for(int i = 0; i < datos; i++){
                    cout<<"Inserte dato: ";
                    cin>>dato;
                    while(dato < 1){
                        cout<<"ingrese solo datos positivos"<<endl;
                        cout<<"Inserte dato: ";
                        cin>>dato;
                    }
                ob.agregarElemento(cabeza, dato);
                }
            }break;

            case 3:
                system ("clear");
                ob.mostrarCola(cabeza);
                break;

            case 4:
                ob.atenderElemento(cabeza);
                break;

            case 5:
                system("clear");
                return 0;

            default:
                cout<<"Ingrese un valor valido"<<endl;
                break;
        }
    }
}