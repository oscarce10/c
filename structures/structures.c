#include <stdio.h>
#include <stdlib.h>
#include "struct.h"
#include <cs50.h>

int main(){
    // Allocate memory for students
    int num = get_int("Enrollment: ");
    student *students = malloc(num * sizeof(student));

    //ask info
    for(int i = 0; i < num; i++){
        printf("Enrollment student %i\n\t", i + 1);
        students[i].name = get_string("Name: ");
        printf("\t");
        students[i].dorm = get_string("Dorm: ");
        printf("\n\n");
    }

    // Show students
    for(int i = 0; i < num; i++){
        printf("Student %i:\n\tName: %s\n\tDorm: %s\n\n", i + 1, students[i].name, students[i].dorm);
    }
}