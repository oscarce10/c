
using namespace std;

class ListaCD{
    int dato;
    ListaCD *sig, *ant;

    public:
    void crearCabeza(ListaCD *&cabeza, int dato);
    void agregarNodo(ListaCD *&cabeza, int dato);
    void crearLista(ListaCD *&cabeza);
    void mostrarLista(ListaCD *cabeza);
    void borrarNodo(ListaCD *&cabeza);
}ListaCD;

void ListaCD::crearCabeza(ListaCD *&cabeza, int dato){
    cabeza = new ListaCD;
    cabeza->dato = dato;
    cabeza->sig = cabeza->ant = cabeza;
}

void ListaCD::agregarNodo(ListaCD *&cabeza, int dato){
    ListaCD *aux = cabeza->ant;
    aux->sig = new ListaCD;
    aux->sig->ant = aux;
    aux = aux->sig;
    aux->dato = dato;
    aux->sig = cabeza;
    cabeza->ant = aux;
}

void ListaCD::mostrarLista(ListaCD *cabeza){
    ListaCD *aux = cabeza;
    cout<<"orden"<<endl;
    do{
        cout<<aux->dato<<" ";
        aux = aux->sig;
    }while(aux != cabeza);
    aux = cabeza->ant;
    cout<<endl<<"Reversa"<<endl;
    do{
        cout<<aux->dato<<" ";
        aux = aux->ant;
    }while(aux != cabeza->ant);
    cout<<endl;
}

void ListaCD::crearLista(ListaCD *&cabeza){
    int dato = 0;
    while(true){
        cout<<"Ingrese dato: ";
        cin>>dato;
        if(dato < 1)
            break;

        (!cabeza)?crearCabeza(cabeza, dato):agregarNodo(cabeza, dato);
    }
}

void ListaCD::borrarNodo(ListaCD *&cabeza){
    ListaCD *aux = cabeza;
    int del;
    cout<<"Inserte dato a borrar: ";
    cin>>del;
    while(aux->dato != del && aux->sig != cabeza){
        aux = aux->sig;
    }
    if(aux->dato == del){
        aux->sig->ant = aux->ant;
        aux->ant->sig = aux->sig;
        if(aux == cabeza){
            cabeza = cabeza->sig;
            cout<<"Cabeza trasladada"<<endl;
        }

        else
            cout<<"Nodo de dato "<<del<<" eliminado"<<endl;

        delete aux;
    }

}