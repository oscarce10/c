// Compile by using g++ -g  circularDouble.cpp  -lcrypt -lcs50 -lm -o circularDouble
#include <iostream>
#include "circularDouble.h"

int main(){
    class ListaCD *cabeza = NULL, ob;

    ob.crearLista(cabeza);
    ob.mostrarLista(cabeza);

    ob.borrarNodo(cabeza);
    ob.mostrarLista(cabeza);

    return 0;
}