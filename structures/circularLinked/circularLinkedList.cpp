// Compile by using g++ -g  circularLinkedList.cpp  -lcrypt -lcs50 -lm -o circularLinkedList

#include "circularLinkedList.h"

int main(){
    class ListaCS *cabeza = NULL, ob;

    ob.crearLista(cabeza);
    ob.mostrarLista(cabeza);

    ob.borrarNodo(cabeza);
    ob.mostrarLista(cabeza);

    return 0;
}