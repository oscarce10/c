#include <iostream>

using namespace std;

class ListaCS{
    int dato;
    ListaCS *sig;

    public:
    void crearCabeza(ListaCS *&, int);
    void agregarNodo(ListaCS *&, int);
    void crearLista(ListaCS *&);
    void mostrarLista(ListaCS *);
    void borrarNodo(ListaCS *&);
}ListaCS;

void ListaCS::crearCabeza(ListaCS *&cabeza, int dato){
    cabeza = new ListaCS;
    cabeza->dato = dato;
    cabeza->sig = cabeza;
}

void ListaCS::agregarNodo(ListaCS *&cabeza, int dato){
    ListaCS *aux = new ListaCS;
    aux->dato = dato;
    aux->sig = cabeza->sig;
    cabeza->sig = aux;
}

void ListaCS::crearLista(ListaCS *&cabeza){
    int num = 0;
    while(true){
        cout<<"Inserte numero: ";
        cin>>num;

        if(num < 1)
            break;

        (!cabeza)?crearCabeza(cabeza, num):agregarNodo(cabeza, num);
    }
}

void ListaCS::mostrarLista(ListaCS *cabeza){
	ListaCS *aux = cabeza;
	do {
		(aux != cabeza)?cout<<aux->dato<<endl:cout<<"--------------\nCabeza\n"<<aux->dato<<"\n--------------\n";
		aux = aux->sig;
	}while (aux!=cabeza);

}

void ListaCS::borrarNodo(ListaCS *&cabeza){
    ListaCS *aux = cabeza, *aux2 = cabeza;
    int del = 0;
    cout<<"Inserte nodo a borrar: ";
    cin>>del;
    while(aux->dato != del && aux->sig != cabeza){
        aux = aux->sig;
    }
    if(aux->dato == del){

        while(aux2->sig != aux){
            aux2 = aux2->sig;
        }

        if(aux == cabeza){
        aux2->sig = aux->sig;
        cabeza = cabeza->sig;
        delete aux;
        cout<<"Nodo de dato "<<del<<" eliminado, cabeza trasladada."<<endl;
        }

        else{
            aux2->sig = aux->sig;
            delete aux;
            cout<<"Nodo de dato "<<del<<" eliminado."<<endl;
        }
    }

}