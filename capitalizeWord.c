#include <stdio.h>
#include <cs50.h>
#include <string.h>

int main(){
	string s = get_string("Digite palabra en minusculas: ");
	for(int i = 0; i<strlen(s); i++){
		if(s[i] >= 'a' &&  s[i] <= 'z'){
			s[i] = s[i] - 32;
		}
	}
	printf("\nPalabra en mayusculas: %s\n", s);
}
